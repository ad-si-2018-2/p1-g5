// Retorna a data e hora atuais formatadas.
function dataAtualFormatada() {
  var dataHoraAtualFormatada = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
  return dataHoraAtualFormatada;
};

module.exports.dataAtualFormatada = dataAtualFormatada();