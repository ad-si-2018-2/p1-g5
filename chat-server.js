// Load the TCP Library
net = require('net');

util = require('./util');

// Define a porta do servidor
var portaServidor = 6677;

// Keep track of the chat clients
var clients = [];

// Canais do Chat Server
var canais = ["#GERAL"];

// Tipos de canais do Chat Server
var tiposCanais = ['&','#','+','!'];

// Message of the day
var motd = "\n[" + util.dataAtualFormatada + "] Seja bem vindo ao Chat Server P1-G5.\nFim do /MOTD.\n";

// Comandos suportados
var comandos = ["NICK","USER","JOIN","LIST","MOTD","PRIVMSG","TIME","PART","QUIT","KILL"];

// User_modes suportados
var userModes = [0,1,2,3,4,5,6,7,8];

// Start a TCP Server
net.createServer(function (socket) {

  // Identify this client
  socket.name = socket.remoteAddress + ":" + socket.remotePort;

  // Put this new client in the list
  clients.push(socket);

  // Mantém o histórico de nicks do usuário.
  socket.nicks = [];

  // Mantém o histórico de nomes do usuário.
  socket.nomes = [];

  // Canal atual do usuário.
  socket.canal = "#GERAL";

  // Send a nice welcome message and announce
  socket.write(motd + "Seu endereço: " + socket.name + "\n");
  broadcast(socket.name + " se juntou ao chat\n", socket);

  // Handle incoming messages from clients.
  socket.on('data', function (data) {
    // broadcast(socket.name + "> " + data, socket);
    analisar(data);
  });

  // Remove the client from the list when it leaves
  socket.on('end', function () {
    clients.splice(clients.indexOf(socket), 1);

    var nick = "";

    if(socket.nick && socket.nick.nick){
      nick = socket.nick.nick;
    } else {
      nick = socket.name;
    }

    broadcast("\n " + nick + " saiu do chat.\n");
  });
  
  // Send a message to all clients
  function broadcast(message, sender) {
    clients.forEach(function (client) {
      // Don't want to send it to sender
      if (client === sender) return;
      client.write(message);
    });
    // Log it to the server output too
    process.stdout.write(message)
  }

  //Envia mensagens para usuarios dos mesmos canais
  function broadcastCanais(message, sender){
      clients.forEach(function(client){
        if(client === sender) return;
        if(client.canal === sender.canal){
            client.write(message);
        }
      });
      // Log it to the server output too
      process.stdout.write(message)
  }

  function analisar(data) { 
    var mensagem = String(data).trim();                                         
    // o método split quebra a mensagem em partes separadas p/ espaço em branco 
    var args = mensagem.split(" ");                                             
    
    if (args && args.length > 0 && comandos.indexOf(args[0].toUpperCase()) > -1) {
      if ( args[0].toUpperCase() == "NICK") {
        novoNick(String(args[1])) ;
      } else if ( args[0].toUpperCase() == "USER") {
        novoUsuario(args);
      } else if ( args[0].toUpperCase() == "JOIN") {
        join(args);
      } else if ( args[0].toUpperCase() == "LIST") {
        listarUsuarios(socket.nick);
      } else if ( args[0].toUpperCase() == "MOTD") {
        getMotd();
      } else if( args[0].toUpperCase() == "PRIVMSG") {
        mensagemPrivada(args);
      } else if( args[0].toUpperCase() == "TIME") {
        socket.write(util.dataAtualFormatada + "\n");
      } else if( args[0].toUpperCase() == "PART"){
        removeDoCanal(args);
      } else if( args[0].toUpperCase() == "QUIT"){
        socket.end();
      } else if(args[0].toUpperCase() == "KILL"){
        kill(args);
      }
    } else {
      //Se o usuário não tiver nenhum nick registrado
      if(socket.nicks.length <= 0) {
        
        broadcastCanais(socket.name + " disse: " + mensagem + "\n",socket);
      }
      else{
        broadcastCanais(socket.nicks[socket.nicks.length -1].nick + ": " + mensagem + "\n",socket);
      }
    }
  }
  
  // Remove usuario passado por parametro do servidor
  function kill(args){
    if(args.length < 2){
      socket.write("Comando KILL: ERR_NEEDMOREPARAMS, numero de parametros incorreto\n");
      return;
    }
    var client = procuraUser(args[1]);
    if(client === null){
      socket.write("Comando KILL: ERR_NOSUCHNICK, usuario nao encontrado\n");
      return;
    }
    client.destroy();
    socket.write("Comando KILL: OK, comando executado com sucesso\n");
  }

  // Remove usuario do canal
  function removeDoCanal(args){
    if(socket.canal !== args[1].toUpperCase()){
        socket.write("Comando PART: ERR_NOSUCHCHANNEL, você nao esta no canal informado\n");
        return;
    }
    socket.canal = "#GERAL";
    socket.write("Comando PART: OK, voce foi removido do canal: "+args[1]+"\n");
  }

  // Procura um usuario pelo nick
  function procuraUser(nick){
    if(!nick.isNaN){
      for (var i = clients.length - 1; i >= 0; i--){
        if(clients[i].nicks[clients[i].nicks.length -1].nick == nick){
          return clients[i];
        }
      }
    }
  }

  // Envia uma mensagem privada para um usuario
  function mensagemPrivada(args){
    var cliente = procuraUser(args[1]);
    if(cliente !== null){
      if(socket.nicks.length <= 0){
        cliente.write("Comando PRIVMSG: Mensagem de " + socket.name + ": " + args[2] + "\n");
      }
      else{
        cliente.write("Comando PRIVMSG: Mensagem de " + socket.nicks[socket.nicks.length -1].nick + ": " + args[2] + "\n");
      }
    }
    else{
      socket.write("Comando PRIVMSG: ERR_NOSUCHNICK" + '[' + util.dataAtualFormatada + '] Usuário não encontrado: ' + args[1] + '\n');
    }
  }

  function novoNick(nick) {
    if (validarNick(nick)) {
      socket.nick = {nick: nick, data : util.dataAtualFormatada};

      socket.nicks.push(socket.nick);

      socket.write("Novo nick inserido com sucesso: " + nick + "\n");
    }      
  }                                                                             
                                                                                
  function novoUsuario(args) {
    if(validarNick(socket.nick)) {
      var cli = clients.filter(c => {
        return (c.nick && c.nick.nick === args[1]);
      });

      if(cli && args[1] === socket.nick.nick) {
        if( (!isNaN(parseInt(args[2])) && isFinite(args[2])) && userModes.indexOf(parseInt(args[2])) > -1) {
          if(args[4]){
            socket.nome = '';
            for (var i = 4; i < args.length; i++) {
              socket.nome = socket.nome + ' ' + args[i];
            }

            socket.nome = {nome : socket.nome, data : util.dataAtualFormatada};

            socket.nomes.push(socket.nome);

            socket.userMode = args[2];

            socket.write('[' + util.dataAtualFormatada + '] Usuário: ' + socket.nome.nome + ' definido com sucesso!\nUser_mode: ' + socket.userMode + '\n');
          } else {
            socket.write('[' + util.dataAtualFormatada + '] O nome do usuário deve ser informado\n');
          }
        } else {
          socket.write('[' + util.dataAtualFormatada + '] UserMode inválido: ' + args[2] + '\n');
        }
      } else {
        socket.write('[' + util.dataAtualFormatada + '] Usuário não encontrado: ' + args[1] + '\n');
      }
    } else {
      socket.write('[' + util.dataAtualFormatada + '] Usuário não encontrado: ' + args[1] + '\nÉ necessário definir seu nick, antes desta operação\n');
    }
  }
                                                                                
  function join(args) {
    var tipoCanal = args[1].charAt(0);
    var canal = args[1].toUpperCase();
    if(tiposCanais.indexOf(tipoCanal) != -1){
      if(socket.canal === canal){
        socket.write("Comando JOIN: Voce ja esta neste canal.\n");
        return;
      }
      else if(canais.indexOf(canal) != -1){
        socket.canal = canal;
        socket.write("Comando JOIN: OK, executado com sucesso\n");
        return;
      }else{
        canais.push(canal);
        socket.canal = canal;
        socket.write("Comando JOIN: OK, executado com sucesso\nA sala foi criada com sucesso!\n");
        return;
      }
    } else{
      socket.write("Comando JOIN: ERR_BADCHANNELKEY\nTipo de canal incorreto! Tipos de canais possiveis:\n");
      for(var i = 0;i < tiposCanais.length;i++){
          socket.write(tiposCanais[i]+" ");
      }
      socket.write("\n");
      return;
    }
  }

  function listarUsuarios() {
    socket.write('### Lista de Usuários ###\n');                                                
    for (var i = clients.length - 1; i >= 0; i--) {
      socket.write((i+1) + ' - ' + clients[i].nick.nick + '\n');
    }               
  }

  function getMotd() {                                                         
    socket.write(motd);                     
  }

  function validarNick(nick){
    var isValid = true;
    if(!nick){
      isValid = false;
      socket.write('[' + util.dataAtualFormatada + '] Nick inválido, pois está vazio.\n');
    } else if(comandos.indexOf(nick) > -1) {
      isValid = false;
      socket.write('[' + util.dataAtualFormatada + '] Nick inválido, pois é um comando: ' + nick + '\n');
    } else {
      clients.forEach(function (client) {
        if(client.nick && client.nick.nick === nick) {
          isValid = false;
          socket.write('[' + util.dataAtualFormatada + '] Nick inválido, pois já está sendo utilizado: ' + nick + '\n');
        }
      });
    }

    return isValid;
  }

}).listen(portaServidor);

// Put a friendly message on the terminal of the server.
console.log('[' + util.dataAtualFormatada + '] Chat server executado na porta: ' + portaServidor + '\n');
